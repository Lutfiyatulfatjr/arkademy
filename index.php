<!doctype html>
<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $db = "arkademy";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $db);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    } 
?>
<html>

<head>
<title>Welcome</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.8/semantic.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.8/semantic.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
</head>
<body>

    <div class="ui inverted segment" style="
        top: 0;
        width: 100%;">Arkademy Bootcamp online Selection - Lutfiyatul Fatjriyati Anas
        
    </div>

    <div id="result" style="margin-top:6%">
        <table class="ui single line table" id="resulttable">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Jumlah Product</th>
                </tr>
            </thead>
            <tbody>
            <?php
                $sql="select product_categories.id, product_categories.name, count(products.category_id) AS Jumlah_Product from product_categories, products where product_categories.id = products.category_id GROUP BY product_categories.id";
                $result = $conn->query($sql);
                $category=array();
                $rows = mysqli_num_rows($result);
                while ($rows = mysqli_fetch_array($result)){
                    echo "<tr>";
                    echo "<td>".$rows['id']."</td>";
                    echo "<td>".$rows['name']."</td>";
                    echo "<td>".$rows['Jumlah_Product']."</td>";
                }
            ?>
            </tbody>
        </table>
    </div>

</body>
<script>
    function showProduct(){
        var itemId = "12341822";
        var itemName = "Basic T-Shirt";
        var price = 70000;
        var availColorandSize  = [["red", "S,M,L"], ["solid black", "M,L"]];
        var freeShipping = false;
        var product = {"itemId":itemId, "itemName":itemName, "price":price, "availColorandSize":availColorandSize, "freeShipping":freeShipping};
        return product;
    } 

    function username_validation(username){
        var ket;
        if (username.length!=8){
            ket="username must be 8 char lengths";
        }else{
            if(username.slice(0,4)==username.slice(0,4).toLowerCase()){
                if(username.charAt(5)=="_"|| username.charAt(5)=="."){
                    if(username.slice(6,7)==username.slice(6,7).toUpperCase()){
                        ket="username approved!";
                    }else{
                        ket="Last 2 character must be at Upper Case";
                    }
                }else{
                    ket="6th character must be _ or . "
                }
            }else{
                ket="5 first characters must be lower case";
            }
        }
        return ket;
    }

    function segitiga(number){
        var output="";
        for(var i=1; i<=number; i++){
            output = output + i +", ";
            document.write(output+"<br>");
        }    
    }

    function count_handshake(number){
        var count=0;
        for(var i=number; i>0; i--){
            count = count + (i-1);
        }
        return count;
    }

    function myCountChar(string1, string2){
        var count=0;
        for(var i=0; i<string1.length; i++){
            if(string1.charAt(i)==string2){
                count++;
            }
        }
        return count;
    }

    
</script>



</html>